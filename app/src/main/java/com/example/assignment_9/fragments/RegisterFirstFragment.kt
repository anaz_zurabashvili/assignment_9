package com.example.assignment_9.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.assignment_9.R
import com.example.assignment_9.databinding.FragmentRegisterFirstBinding
import com.example.assignment_9.extensions.emailValid
import com.example.assignment_9.extensions.hideKeyboard
import com.example.assignment_9.extensions.showSnackBar


class RegisterFirstFragment : Fragment() {

    private lateinit var binding: FragmentRegisterFirstBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterFirstBinding.inflate(inflater, container, false)
        setListeners()
        return binding.root
    }

    private fun setListeners() {
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
        binding.btnNext.setOnClickListener(btnNextClick)
    }

    private fun isAllFieldsValid(): Boolean = !(binding.etEmailAddress.text.isNullOrBlank() ||
            binding.etPassword.text.isNullOrBlank())

    private fun makeValidation(): Boolean = isAllFieldsValid()  &&
            binding.etEmailAddress.text.toString().emailValid()

    private val btnNextClick = View.OnClickListener {
        if (makeValidation()) {
            val bundle = Bundle()
            bundle.putString("email" , binding.etEmailAddress.text.toString().trim())
            bundle.putString("password" , binding.etEmailAddress.text.toString().trim())
            findNavController().navigate(R.id.action_registerFirstFragment_to_registerSecondFragment, bundle)
        } else {
            it.hideKeyboard()
            it.showSnackBar("Validation Error. Try again!")
        }

    }
}