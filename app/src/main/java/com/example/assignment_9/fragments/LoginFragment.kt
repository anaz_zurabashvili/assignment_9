package com.example.assignment_9.fragments

import android.os.Bundle
import android.util.Log.d
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.assignment_9.databinding.FragmentLoginBinding
import com.example.assignment_9.extensions.*
import com.google.firebase.auth.FirebaseAuth

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        authSignOut()
        setListeners()
    }
    private fun authSignOut(){
        if (auth.currentUser != null){
            auth.signOut()
        }
    }

    private fun isAllFieldsValid(): Boolean = !(binding.etEmailAddress.text.isNullOrBlank() ||
            binding.etPassword.text.isNullOrBlank())

    private fun makeValidation(): Boolean = isAllFieldsValid() &&
            binding.etEmailAddress.text.toString().emailValid()

    private fun setListeners() {
        binding.btnBack.setOnClickListener {
            auth.signOut()
            findNavController().popBackStack()
        }
        binding.btnLogin.setOnClickListener(btnLoginClick)
    }

    private val btnLoginClick = View.OnClickListener {
        auth.signOut()
        if (makeValidation()) {
            auth.signInWithEmailAndPassword(
                binding.etEmailAddress.text.toString(),
                binding.etPassword.text.toString()
            )
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        it.hideKeyboard()
                        it.showSnackBar( "${getString(STRINGS.unsuccessful_sign_in)} ${task.result}")
                        binding.tvStatus.text = "${getString(STRINGS.successful_sign_in)} ${auth.currentUser?.email}"
                        binding.tvStatus.visible()
                    } else {
                        it.hideKeyboard()
                        it.showSnackBar("${getString(STRINGS.unsuccessful_sign_in)} ${task.exception?.message}")
                    }
                }
        } else {
            it.hideKeyboard()
            it.showSnackBar(getString(STRINGS.validation_error))
        }
    }

}