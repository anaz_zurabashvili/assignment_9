package com.example.assignment_9.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.assignment_9.databinding.FragmentRegisterSecondBinding
import com.example.assignment_9.extensions.STRINGS
import com.example.assignment_9.extensions.hideKeyboard
import com.example.assignment_9.extensions.showSnackBar
import com.example.assignment_9.extensions.visible
import com.google.firebase.auth.FirebaseAuth

class RegisterSecondFragment : Fragment() {
    private lateinit var binding: FragmentRegisterSecondBinding
    private lateinit var email: String
    private lateinit var password: String

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentRegisterSecondBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        setListeners()
        auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null){
            auth.signOut()
        }
        email = arguments?.getString("email") ?: ""
        password = arguments?.getString("password") ?: ""
        authSignOut()
    }

    private fun setListeners() {
        binding.btnBack.setOnClickListener(btnBackClick)
        binding.btnSignUp.setOnClickListener(btnSignUpClick)
    }

    private val btnBackClick = View.OnClickListener {
        authSignOut()
        findNavController().popBackStack()
    }

    private fun authSignOut(){
        if (auth.currentUser != null){
            auth.signOut()
        }
    }

    private fun makeValidation(): Boolean = !(binding.etName.text.isNullOrBlank())

    private val btnSignUpClick = View.OnClickListener {
        auth.signOut()
        if (makeValidation()) {
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        it.hideKeyboard()
                        it.showSnackBar("${getString(STRINGS.successful_sign_up)} ${task.result}")
                        binding.tvStatus.text =
                            "${getString(STRINGS.successful_sign_up)} ${auth.currentUser?.email}"
                        binding.tvStatus.visible()
                    } else {
                        it.hideKeyboard()
                        it.showSnackBar("${getString(STRINGS.unsuccessful_sign_up)} ${task.exception?.message}")
                        binding.tvStatus.text =
                            "${getString(STRINGS.unsuccessful_sign_up)} ${task.exception?.message}"
                        binding.tvStatus.visible()
                    }
                }
        } else {
            it.hideKeyboard()
            it.showSnackBar("Validation Error. Try again!")
        }
    }

}