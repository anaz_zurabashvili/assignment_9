package com.example.assignment_9.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make


fun View.showSnackBar(title:String)=
    make(this, title, Snackbar.LENGTH_LONG).show()


fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun View.gone() = View.GONE.also { visibility = it }

fun View.visible() = View.VISIBLE.also { visibility = it }
